app.service('Feedback', ["$http","$q","$rootScope", function($http,$q,$rootScope){

    this.send = function(email,content){
        var defered = $q.defer();
        var req = {
            method: 'POST',
            url: apiUrl+'sendFeedback/',
            data: '?token=42&email='+email+'&content='+content,
            headers : {
                'Content-Type' : 'application/x-www-form-urlencoded'
            },
            transformResponse: function (data, headersGetter, status) {
                return {data: data};
            }
        }
        $http(req).success(function(data) {
            defered.resolve();
        }).error(function(e){
            defered.reject();
        });

        return defered.promise;

    }

}]);