app.controller('startCtrlr', ['$rootScope','$scope', '$location','$timeout','LocalStorage','WebSQL','Start', 'toastr',
    function($rootScope,$scope, $location,$timeout, LocalStorage,WebSQL,Start,toastr) {

    LocalStorage.getDate().then(function(data){
         $rootScope.dateUpdate = data;
        if($rootScope.dateUpdate == null){
            //on créer les tables
            if($rootScope.lang == 'fra'){
                toastr.info('Importation des mots en cours');
            } else {
                toastr.info("Words importation in progress");
            }
            WebSQL.createTables().then(function(){
                Start.getAllData().then(function(data){
                    WebSQL.addAllWord(data['word']);
                    WebSQL.addAllCateg(data['categ']);
                    LocalStorage.setDate();
                    $scope.selectAllData();
                });
            });
        }else{
             Start.updateData($rootScope.dateUpdate).then(function(results){
                if(results.words != "" || results.categ.length > 0){
                    WebSQL.updateWord(results.word);
                    WebSQL.updateCateg(results.categ);
                    LocalStorage.setDate();
                    if($rootScope.lang == 'fra'){
                        toastr.info('Mise à jour réussie');
                    } else {
                        toastr.info("Update completed");
                    }
                }
                    $scope.selectAllData();
             }, function(){
                $scope.selectAllData();
             });

             //on check les majs
        }
        //GENRE ICI
        //
        $scope.selectAllData = function(){
            $scope.getLang();
            WebSQL.selectAllCateg().then(function(results){
                $rootScope.tabCategs = results;
                WebSQL.selectAllWord().then(function(results){
                    // $rootScope.tabWords = $rootScope.orderArrayByLetter(results);
                    $rootScope.tabWords =results;
                    WebSQL.selectAllFavList().then(function(results){
                        $rootScope.tabFavList = results;
                        $timeout(function(){
                            $location.path('/home');//page de démarrage
                        },1000);
                     });
                 });
             });
        }

        $scope.getLang = function(){
            LocalStorage.getLang().then(function(data){
                $rootScope.lang = data;

            }, function(){
                $rootScope.lang ='fra';
            });
        }

    });

}]);
