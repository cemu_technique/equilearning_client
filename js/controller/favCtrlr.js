app.controller('favCtrlr', ['$rootScope','$scope', '$location','LocalStorage','WebSQL','toastr','ngDialog',
    function($rootScope,$scope, $location, LocalStorage, WebSQL,toastr, ngDialog) {
    window.scrollTo(0,0);
    $scope.goToFavList = function(id){
        $location.path('favList/'+id);
    }
    $scope.showDeleteFavList = function(fav){
        $scope.currentFavList = fav;
        ngDialog.open({
            template: 'partial/templates/confirmDeleteFavList.html',
            className: 'ngdialog-theme-plain',
            scope: $scope,
            controller : ['$scope', 'WebSQL', function($scope, WebSQL) {
                $scope.deleteFavList = function(){
                    WebSQL.deleteFavList($scope.currentFavList.id);
                    $scope.selectAllList();
                    if($rootScope.lang == 'fra'){
                        toastr.success('Liste supprimée.');
                    } else {
                        toastr.success('List deleted.');
                    }
                    $scope.closeThis();

                }
                $scope.closeThis = function()
                {
                    ngDialog.close(0);
                }
            }]
        });
    }
    $scope.selectAllList = function(){
        WebSQL.selectAllFavList().then(function(data){
            $rootScope.tabFavList = data;
        });
    }
    $scope.addList = function(){
        if($scope.nameList.length > 0){
            WebSQL.createFavList($scope.nameList);
            $scope.selectAllList();
            $scope.nameList = "";
            if($rootScope.lang == 'fra'){
                toastr.success('Liste créée');
            } else {
                toastr.success('List created');
            }
        }

    }

    $scope.selectAllList();
}]);
