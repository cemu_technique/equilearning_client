Au passage à la version 1.1 (sous IOS), par Guillaume Brosse, plusieurs modifications ont été effectuées (notamment l'ajout d'un splash screen).
L'étendue de ces modificvations nous a convaincu de publier, non seulement le coeur de l'application (dossiers img, js, partial...), mais aussi le reste de l'application cordova (dossier iosapp).

Le dossier iosapp est donc le fichier de TOUTE l'application cordova sous IOS de Guillaume, au départ tel qu'il nous l'a donné au moment du passage en 1.1.

La description que Guillaume fait de ses changements est la suivante :
______________________________________________________________________________________________________
Et les instructions ci-dessous pour les modifications hors code (sauf les points A et B qui sont deux corrections importantes que je vous indique donc) : 

A : Lancer la commande 
	$ cordova plugin add cordova-plugin-statusbar
	Puis ajouter ceci dans config.xml
		<preference name="StatusBarOverlaysWebView" value="false" />
    		<preference name="StatusBarBackgroundColor" value="#000000" />
    		<preference name="StatusBarStyle" value="lightcontent" />
    		<gap:plugin name="com.phonegap.plugin.statusbar" />

B : Pour le problème de son : dans le fichier situé dans www s’appelant index.html, remplacer :
 <audio src="" style="display:none;" id="audio" preload="auto" controls></audio>
par 
 <audio src="res/audio/1sec.mp3" style="display:none;" id="audio" preload="auto" controls></audio>



—— XCODE ——
1 : Dans Build Settings, Apple LLVM 8.0 - Preprocessing
—> Deplier preprocessor macros et ajouter dans release : DISABLE_PUSH_NOTIFICATIONS=1

2 : Dans les problèmes affiches dans XCODE, cliquer sur les deux premiers (ceux qui ne sont pas dans cordova lib) et cliquer sur ‘perform changes’

3 : Ajouter dans ‘build settings’ -> ‘ pre processeur macros’ ajouter : DISABLE_PUSH_NOTIFICATIONS=1

4 : Changer le parametre dans General —> App Icon And Launch Images —> Launch Screen File et choisir CDVLaunchScreen
5 : Changer le parametre dans General —> App Icon And Launch Images —> LaunchImages Source et choisir LaunchImage
6 : Changer le parametre dans General —> Deployment Info —>Deployment Target et choisir 8.0

7 : Dans Images.XCAssets
	—> Choisir AppIcon et glisser déposer les icônes correspondants en fonctions des bonnes tailles (si la taille ne correspond pas, un warning apparaîtra)
	—> Choisir LaunchImage et glisser déposer les splashscreen correspondants en fonctions des bonnes tailles (si la taille ne correspond pas, un warning apparaîtra)
	—> Choisir LaunchStoryBoard et glisser déposer les splashscreen correspondants en fonctions des bonnes tailles (si la taille ne correspond pas, un warning apparaîtra)

Toutes les autres modifications sont dans le code.

