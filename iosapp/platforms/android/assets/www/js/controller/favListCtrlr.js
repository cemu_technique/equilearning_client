app.controller('favListCtrlr', ['$rootScope','$scope', '$location','LocalStorage','WebSQL','$routeParams','$timeout',
    function($rootScope,$scope, $location, LocalStorage, WebSQL,$routeParams,$timeout) {
        window.scrollTo(0,0);
    	WebSQL.selectFavList($routeParams.idFav).then(function(results){
    		$scope.favList=results;
    		$scope.selectAllWordInFavList();
    	});

        WebSQL.selectCategFavList($routeParams.idFav).then(function(results){
            $scope.tabCategsFavList = results;
        });

        $scope.currentCateg = null;

        $scope.$watch('searchWord', function(newValue, oldValue){
            $scope.hideLetterEmpty();
        });
        $scope.$watch('currentCateg', function(newValue,oldValue){
            $scope.hideLetterEmpty();
        });

    	$("body").on('click','.dico button',function(){
	        var value = $(this).text();
	        $('html,body').animate({scrollTop: $("."+value).offset().top - 40}, 'fast');
    	});

        $scope.displayCateg = function(currentCateg){
            if(currentCateg!= null) {
                $rootScope.currentCateg = currentCateg.id;
            } else {
                $rootScope.currentCateg = null;
            }
            $scope.openCateg = false;
        }

        $scope.hideLetterEmpty = function(){
            $timeout(function(){
                $('.letter').each(function(key){
                     var letterDico = $(this).find('.content').find('h1').text();
                    if($(this).find('.mot').length == 0) {
                        $(this).hide();
                        if(letterDico != "" && letterDico != " "){
                            $(".dico ."+letterDico).hide();

                        }
                    } else {
                        $(this).show();
                        if(letterDico != "" && letterDico != " "){
                            $('.dico .'+letterDico).show();
                        }
                    }

                });

            },200);
        }

    	$scope.selectAllWordInFavList = function(){
    		WebSQL.selectFavListWords($routeParams.idFav).then(function(results){
	    		$scope.favListWords = $rootScope.orderArrayByLetter(results);
    		});
    	}

    	$scope.deleteWordInTabList = function(word){
             if($rootScope.lang == 'fra'){
                var msg = 'Supprimer cette liste ?';
            } else {
                var msg = 'Delete this list ?';
            }
            if(confirm(msg)){
        		WebSQL.deleteWordInFavList($scope.favList.id,word.id);
        		$scope.selectAllWordInFavList();
            }
    	}
}]);
