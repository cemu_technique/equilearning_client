app.controller('feedbackCtrlr', ['$rootScope','$scope', '$location','LocalStorage','Feedback','toastr',
    function($rootScope,$scope, $location, LocalStorage, Feedback, toastr) {
        window.scrollTo(0,0);
    	$scope.infobar = false;
    	$scope.send = function(){
    		if($scope.email != "" && $scope.content != "") {
    			Feedback.send($scope.email,$scope.content).then(function(){
                    if($rootScope.lang == 'fra'){
                            toastr.success('Votre message a été envoyé avec succès.');
                        } else {
                            toastr.success('Your message has been sent.');
                        }
                }, function(){
                    if($rootScope.lang == 'fra'){
                            toastr.error('Erreur, merci de réessayer plus tard.');
                        } else {
                            toastr.success('An error occurred. Please try again later.');
                        }
                });
    			$scope.infobar = true;
    			$scope.email = '';
    			$scope.content = '';
    		}else {
				 if($rootScope.lang == 'fra'){
                	toastr.error("Votre email et votre message doivent être renseignés.");
				 } else {
					toastr.error("Your email and your message must be filled in to process the data.");
				 }
            }
    	}

}]);
