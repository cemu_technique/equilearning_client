'use strict';

var app = angular.module('app', ['ngRoute','ngAnimate','ngDialog','infinite-scroll','toastr']);

var apiUrl = "http://equilearning.unicaen.fr/";
var soundUrl = "http://equilearning.unicaen.fr/uploads/documents";

app.config(['$routeProvider', function($routeProvider) {

	$routeProvider
	  	.when('/', {
	  		templateUrl: 'partial/start.html',
	  		controller: 'startCtrlr'
	  	})
        .when('/lang', {
            templateUrl: 'partial/lang.html',
            controller: 'langCtrlr'
        })
        .when('/home', {
            templateUrl: 'partial/home.html',
            controller: 'homeCtrlr'
        })
        .when('/fav', {
            templateUrl: 'partial/fav.html',
            controller: 'favCtrlr'
        })
        .when('/favList/:idFav',{
            templateUrl : 'partial/favList.html',
            controller : 'favListCtrlr'
        })
        .when('/feedback',{
            templateUrl : 'partial/feedback.html',
            controller: 'feedbackCtrlr'
        })
        .when('/about', {
            templateUrl: 'partial/about.html',
        })
	    .otherwise({
	    	redirectTo: '/'
	    });
	}

]);
app.config(['toastrConfig', function (toastrConfig) {
  angular.extend(toastrConfig, {
    allowHtml: true,
    closeButton: false,
    closeHtml: '<button>&times;</button>',
    containerId: 'toast-container',
    extendedTimeOut: 1000,
    iconClasses: {
      error: 'toast-error',
      info: 'toast-info',
      success: 'toast-success',
      warning: 'toast-warning'
    },
    messageClass: 'toast-message',
    positionClass: 'toast-bottom-full-width',
    tapToDismiss: true,
    timeOut: 1000,
    titleClass: 'toast-title',
    toastClass: 'toast'
  });
}]);
angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 1000);
app.run(['$rootScope','$location','WebSQL', function ($rootScope, $location, WebSQL) {

    $location.path('/');
    $("audio").on("error", function (e) {
        if($rootScope.lang== 'fra'){
            var msg = "Son introuvable";
        } else {
            var msg = "No sound found";
        }
        alert(msg);
    });
    $rootScope.playSound = function(word){
        console.log("PLaySound");
        if($rootScope.lang == 'fra'){
            console.log(word.enSound);
            $('audio').attr('src',soundUrl+'/'+word.enSound);
        } else {
            console.log(word.frSound);
            $('audio').attr('src',soundUrl+'/'+word.frSound);
        }
        $('#audio')[0].play();


    }

    $rootScope.orderArrayByLetter = function(results) {
        var currentLetter = null;
        var arrayReturn=[];
        for(var i=0; i<results.length; i++) {
            if($rootScope.lang == 'fra'){
                if(currentLetter == null || currentLetter != results[i].frWord.substr(0,1)) {
                    currentLetter = results[i].frWord.substr(0,1);
                    arrayReturn.push({
                        'letter':currentLetter,
                        'words': []
                    });
                }
            } else {
                if(currentLetter == null || currentLetter != results[i].enWord.substr(0,1)) {
                    currentLetter = results[i].enWord.substr(0,1);
                    arrayReturn.push({
                        'letter':currentLetter,
                        'words': []
                    });
                }
            }
            arrayReturn[arrayReturn.length-1].words.push(results[i]);

        }

        return arrayReturn;

    }

    $rootScope.goTo = function(path){
        $location.path(path);
    }
}]);

app.filter('categFilter', function() {

  // In the return function, we must pass in a single parameter which will be the data we will work on.
  // We have the ability to support multiple other parameters that can be passed into the filter optionally
  return function(items, idCateg) {
    var filtered=[];
    for(var i=0; i<items.length;i++) {
        if(idCateg == null || idCateg == items[i].idCateg){
            filtered.push(items[i])
        }
    }
    return filtered;

  }

});

app.filter('wordFilter', function() {
    return function(items, pattern) {
    var filtered=[];
    if(pattern.length >=2){
        for(var i=0; i<items.length;i++) {
            if(items[i].frWord.indexOf(pattern) != -1 || items[i].enWord.indexOf(pattern) != -1){
                filtered.push(items[i])
            }
        }
        return filtered;
    }else {
        return items;
    }

  }
});
app.filter('capitalize', function() {
    return function(input, all) {
      return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
});
app.filter('firstLetterFilter', function () {
    return function (input, letter, lang) {
        if(letter != null){
            letter = letter.toLowerCase();
            input = input || [];
            var out = [];
            input.forEach(function (item) {
                    // console.log("current item is", item);

                if (item.frWord.charAt(0).toLowerCase() == letter && lang == 'fra') {

                    out.push(item);
                } else if (item.enWord.charAt(0).toLowerCase() == letter && lang == 'eng'){
                    out.push(item);
                }
            });
            console.log('OUT IS', out);
            return out;
        } else {
            return input;
        }
    }
});