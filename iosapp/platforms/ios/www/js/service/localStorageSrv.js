app.service("LocalStorage", ["$http","$q","$rootScope", function($http,$q,$rootScope){


	this.setLang = function(lang){
		var defered = $q.defer();
		localStorage.setItem("lang",JSON.stringify(lang));
		defered.resolve();
		return defered.promise;
	};

	this.getLang = function(){

		var defered = $q.defer();
		if(localStorage.getItem("lang") === null){
			defered.reject();
		}else{
			var lang = localStorage.getItem("lang");
			lang = JSON.parse(lang);
			defered.resolve(lang);
		}
		return defered.promise;
	};

	this.removeLang = function(){
		localStorage.removeItem("lang");
	};

	this.getDate = function(){
		var defered = $q.defer();
		if(localStorage.getItem("date") === null){
			defered.resolve(null);
		}else{
			var date = localStorage.getItem("date");
			defered.resolve(date);
		}
		return defered.promise;
	};

	this.setDate = function(){
		localStorage.setItem('date', new Date().toISOString().slice(0, 19).replace('T', ' '));
	};

}]);