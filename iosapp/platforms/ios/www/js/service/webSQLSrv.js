app.service('WebSQL', ["$http","$q","$rootScope","$timeout", function($http,$q,$rootScope, $timeout){

    var db = openDatabase('equilearning', '1.0', 'Equilearning APP', 2 * 1024 * 1024);
    var that = this;
    this.createTables = function () {
        var defered = $q.defer();
        db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS  category (id,name,frname)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS  word (id,enWord,frWord,enSound,frSound,idCateg,dateModif)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS favList (id,name)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS wordInList (idFav,idWord)');
            defered.resolve();
        });
        return defered.promise;
    };

    this.addAllCateg = function (tabCateg) {

        db.transaction(function (tx) {
            tx.executeSql('DELETE FROM category');
            for(var i =0; i<tabCateg.length;i++){
                tx.executeSql('INSERT INTO category (id,name,frname) VALUES (?,?,?)', [tabCateg[i].id,tabCateg[i].name, tabCateg[i].frname]);
            }
        });
    };

    this.addAllWord = function (tabWord) {

        db.transaction(function (tx) {
            tx.executeSql('DELETE FROM word');
            for(var i =0; i<tabWord.length;i++){
                tx.executeSql('INSERT INTO word (id,enWord,frWord,enSound,frSound,idCateg,dateModif) VALUES (?,?,?,?,?,?,?)', [tabWord[i].id,tabWord[i].enWord,tabWord[i].frWord,tabWord[i].enSound,tabWord[i].frSound,tabWord[i].idCateg,tabWord[i].dateModif]);
            }
        });

    };

    this.updateWord = function(tabWord) {
         db.transaction(function (tx) {
            for(var i =0; i<tabWord.length;i++){
                tx.executeSql('DELETE FROM word WHERE id = ?', [tabWord[i].id]);
                tx.executeSql('INSERT INTO word (id,enWord,frWord,enSound,frSound,idCateg,dateModif) VALUES (?,?,?,?,?,?,?)', [tabWord[i].id,tabWord[i].enWord,tabWord[i].frWord,tabWord[i].enSound,tabWord[i].frSound,tabWord[i].idCateg,tabWord[i].dateModif]);
            }
        });
    }
    this.updateCateg = function(tabCateg) {
        db.transaction(function (tx) {
            for(var i =0; i<tabCateg.length;i++){
                tx.executeSql('DELETE FROM category WHERE id = ?', [tabCateg[i].id]);
                tx.executeSql('INSERT INTO category (id,name, frname) VALUES (?,?,?)', [tabCateg[i].id,tabCateg[i].name, tabCateg[i].frname]);
            }
        });
    }

    this.selectAllCateg = function() {
        var defered = $q.defer();
        var tabCateg = [];
        db.transaction(function(tx){
            tx.executeSql('SELECT * FROM category', [], function(tx, results){
                for(var i=0; i<results.rows.length;i++){
                    tabCateg.push(results.rows.item(i));
                }
                defered.resolve(tabCateg);
            });
        });
        return defered.promise;
    };

    this.selectAllWord = function() {
        var defered = $q.defer();
        var tabWord = [];
        db.transaction(function(tx){
            if($rootScope.lang == 'fra'){
               var $req = 'SELECT * FROM word ORDER BY UPPER(frWord)';
            } else {
                var $req = 'SELECT * FROM word ORDER BY UPPER(enWord)';
            }
            tx.executeSql($req, [], function(tx, results){
                for(var i=0; i<results.rows.length;i++){
                    tabWord.push(results.rows.item(i));
                }
                defered.resolve(tabWord);
            });
        });
        return defered.promise;
    };

    this.selectAllFavList = function(){
        var defered = $q.defer();
        var tabFavList = [];
        db.transaction(function(tx){
            tx.executeSql('SELECT * from favList', [], function(tx,results){
               for(var i=0; i<results.rows.length;i++){
                    tabFavList.push(results.rows.item(i));
                }
                defered.resolve(tabFavList);
            });
        });
        return defered.promise;
    };

    this.selectFavList = function(idFav){
        var defered = $q.defer();
        idFav = parseInt(idFav);
        db.transaction(function(tx){
            tx.executeSql('SELECT * from favList WHERE id = ?', [idFav], function(tx,results){
                defered.resolve(results.rows.item(0));
            });
        });
        return defered.promise;
    };

    this.selectFavListWords = function(idFav){
        var defered = $q.defer();
        var tabFavListWords = [];
        idFav = parseInt(idFav);
        db.transaction(function(tx){
            if($rootScope.lang == 'fra'){
               var $req = 'SELECT word.* from wordInList JOIN word ON idWord = word.id WHERE idFav = ? ORDER BY UPPER(word.frWord)';
            } else {
                var $req = 'SELECT word.* from wordInList JOIN word ON idWord = word.id WHERE idFav = ? ORDER BY UPPER(word.enWord)';
            }
            tx.executeSql($req, [idFav], function(tx,results){
               for(var i=0; i<results.rows.length;i++){
                    tabFavListWords.push(results.rows.item(i));
                }
                defered.resolve(tabFavListWords);
            });
        });
        return defered.promise;
    };

    this.selectCategFavList = function(idFav){
        var defered = $q.defer();
        var tabCateg = [];

        idFav = parseInt(idFav);
        db.transaction(function(tx){
            tx.executeSql('SELECT DISTINCT(category.id),category.name from wordInList JOIN word ON idWord = word.id JOIN category ON idCateg = category.id WHERE idFav = ?', [idFav], function(tx,results){
                for(var i=0; i<results.rows.length;i++){
                    tabCateg.push(results.rows.item(i));
                }
                defered.resolve(tabCateg);
            });
        });
        return defered.promise;
    };

    this.createFavList = function(name){
        db.transaction(function(tx){
            tx.executeSql('SELECT max(id) as maxId from favList', [], function(tx,results){
                if(results.rows.item(0).maxId == null){
                    var id = 0;
                }else{
                    var id = results.rows.item(0).maxId +1;
                }
                tx.executeSql('INSERT INTO favList (id,name) VALUES(?,?)', [id,name]);
            });

        });
    };

    this.createFavListAndInsertIn = function(name,word) {
        db.transaction(function(tx){
            tx.executeSql('SELECT max(id) as maxId from favList', [], function(tx,results){
                if(results.rows.item(0).maxId == null){
                    var id = 0;
                }else{
                    var id = results.rows.item(0).maxId +1;
                }
                tx.executeSql('INSERT INTO favList (id,name) VALUES(?,?)', [id,name]);
                that.createWordInFavList(id,word.id);
            });

        });
    };

    this.createWordInFavList = function(idFav, idWord){
        db.transaction(function(tx){
            tx.executeSql('SELECT * from wordInList WHERE idFav = ? AND idWord = ?', [idFav,idWord], function(tx,results){
                if(results.rows.length == 0){
                    tx.executeSql('INSERT INTO wordInList (idFav,idWord) VALUES(?,?)', [idFav,idWord]);
                }
            });

        });
    };

    this.deleteFavList = function(id){
        db.transaction(function(tx){
            tx.executeSql('DELETE FROM favList WHERE id = ?', [id]);
            tx.executeSql('DELETE FROM wordInList WHERE idFav = ?', [id]);
        });
    };

    this.deleteWordInFavList = function(idFav, idWord){
        db.transaction(function(tx){
            tx.executeSql('DELETE FROM wordInList WHERE idFav = ? AND idWord = ?', [idFav, idWord]);
        });
    };

    this.dataTest = function(){
        db.transaction(function(tx){
            tx.executeSql('INSERT INTO favList (id,name) VALUES(0,"test")');
            tx.executeSql('INSERT INTO favList (id,name) VALUES(1,"Ma liste à moi")');
            tx.executeSql('INSERT INTO favList (id,name) VALUES(2,"Ma liste à moi 2")');

        });
    }
}]);