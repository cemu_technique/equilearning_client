# README #

**ATTENTION !!!** Ce dépot est redondant avec le depot [demo/appli](https://bitbucket.org/cemu_technique/equilearning_demo/appli).

La partie redondante dans l'autre dépôt a été supprimée au profit de celle-ci (a ne plus considérer comme obsolète maintenant).

Commits communs (avant divergence des contenus) :

* equilearning_demo/appli = 31b692018981d996c4e7ce9cf65acac7626b40db
* equilearning_client = 7ecc429d7195aa71e669dfafb0addd17ecfa5d4b

## Présentation ##

Ce dépôt contient le code source de la partie applicative (front end) de l'application Equilearning. Elle est commune à 

* La démo web (voir dépôt [equilearning_demo](https://bitbucket.org/cemu_technique/equilearning_demo))
* l'application Android
* l'application IOS

## Points d'amélioration ##
Dans la perspective d'une future version, il faudrait :

* Dissocier les concepts de Langue de l'utilisateur et de Sens de traduction (anglais -> français ou francais -> anglais)
* Ajouter un item quitter dans le menu (l'application est difficile a fermer)
* Revoir la structure du programme (ne pas redémarrer l'application à chaque changement de sens de traduction, pour pouvoir mettre la page de langue en accueil au démarrage)
* Continuer le travail sur la recherche insensible aux accents (voir branche recherche_insensible_accent)
* Personnaliser le splash screen (au moins pour Android)
* Corriger le problème d'affichage sur certains mobiles ou la liste des mots apparaît avec plusieurs mots sur la même ligne (vu sur le smartphone Android d'Anne Garnavault Samsung mini S3GT-18190M Android 4.1.2, kernel 3.0.31)

## Compilation ##

La compilation des exécutables pour mobiles se fait grâce à cordova

### Compilation pour Android ###

Voir "lancement_de_la_compilation" dans le wiki unicaen

### Compilation pour IOS ###

A écrire...

## Contact ##

*cemu.technique@xxxx*

Avec *xxxx* = domaine université Caen