app.service('Start', ["$http","$q","$rootScope", function($http,$q,$rootScope){

    this.getAllData = function(){
        var defered = $q.defer();
        var req = {
            method: 'POST',
            url: apiUrl+'initApp/',
            headers : {
                'Content-Type' : 'application/x-www-form-urlencoded'
            }
        }
        console.log(req.url);

        $http(req).success(function(data) {
            defered.resolve(data);

        }).error(function(){
            defered.reject();
        });

        return defered.promise;
    }

    this.updateData = function(date){
        var defered = $q.defer();
        date = date.split(' ');
        var req = {
            method: 'POST',
            url: apiUrl+'updateApp/'+date[0],
            headers : {
                'Content-Type' : 'application/x-www-form-urlencoded'
            }
        }

        $http(req).success(function(data) {
            defered.resolve(data);

        }).error(function(){
            defered.reject();
        });

        return defered.promise;
    }

}]);