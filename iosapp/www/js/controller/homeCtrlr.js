app.controller('homeCtrlr', ['$rootScope','$scope', '$location','$timeout','$filter','$q','LocalStorage','WebSQL','ngDialog','toastr',
    function($rootScope,$scope, $location,$timeout, $filter,$q, LocalStorage, WebSQL,ngDialog,toastr) {

    $scope.loadMoreOn = true;
    $scope.goToLetter = function(letter){
        $scope.loaderHidden = false;
        $scope.currentLetter = letter;
        $scope.goToLetterPromise(letter).then(function(){
            $timeout(function(){
                $scope.loaderHidden=true;
            },1000);
        });
    }


    $scope.goToLetterPromise = function(letter) {
        var defered = $q.defer();
        if(letter != null){
            $scope.tabWordsHome = $filter('firstLetterFilter')($scope.tabWords, letter, $rootScope.lang);
            $scope.loadMoreOn = false;
        } else {
            $scope.tabWordsHome = [];
            $scope.loadMoreOn = true;
            $scope.loadMore();
        }
        $timeout(function(){
            $scope.showLetterTitle();
        });
        $('html,body').animate({scrollTop: 0}, 'medium');
        defered.resolve();
        return defered.promise;
    }

    $scope.displayCateg = function(currentCateg){
        if(currentCateg!= null) {
            $rootScope.currentCateg = currentCateg.id;
        } else {
            $rootScope.currentCateg = null;
        }
        console.log($rootScope.currentCateg);
        $scope.openCateg = false;
        $scope.goToLetter(null);
    }

    $scope.hideLetterEmpty = function(){
        $timeout(function(){
           var currentLetter = null;
           $scope.tabLetters = [];

           angular.forEach($scope.tabWords, function(value,key){
                if($rootScope.lang == 'fra') {
                    var thisLetter = value.frWord.charAt(0).toUpperCase();
                } else {
                    var thisLetter = value.enWord.charAt(0).toUpperCase();
                }
                // console.log('THIS LETTER'+ thisLetter + 'CurrentLETTER' + currentLetter);
                if(currentLetter == null || currentLetter != thisLetter)
                {
                    currentLetter = thisLetter;
                    $scope.tabLetters.push({
                        'id': $scope.tabLetters.length,
                        'letter': currentLetter
                    });
                }
           });
           $scope.showLetterTitle();
        });
    }

    $scope.showLetterTitle = function(){
        $timeout(function(){
            var currentLetter = null;
            $('.letter').remove();
            $('.mot').each(function(key){
                if(currentLetter == null || currentLetter != $(this).find('h3').text().charAt(0).toUpperCase()){
                    currentLetter = $(this).find('h3').text().charAt(0).toUpperCase();
                    $('<div class="letter listAnimate '+currentLetter+'"><h1 class="h1">'+currentLetter+'</h1><hr class="hr"></div>').insertBefore(this);
                }
            });
        },100);
    }

    $scope.showDialogAddWordInFavList = function(word) {
        $scope.wordNgDialog = word;
        ngDialog.open({
            template: 'partial/templates/addWordFavList.html',
            className: 'ngdialog-theme-plain',
            scope: $scope,
            controller : ['$scope', 'WebSQL', function($scope, WebSQL) {
                $scope.addList = function(word){
                    if($scope.nameList.length > 0){
                        WebSQL.createFavListAndInsertIn($scope.nameList,word);
                        $scope.nameList = "";
                        $scope.infoBar= true;
                        $scope.newList = false;
                        WebSQL.selectAllFavList().then(function(data){
                            $rootScope.tabFavList = data;
                        });
                        if($rootScope.lang == 'fra'){
                            toastr.success('Ajouté avec succès à la liste');
                        } else {
                            toastr.success('Successfully added to the list');
                        }
                        ngDialog.close();

                    }

                }
            }]
        });
    }

    $scope.addWordInFavList = function(favList,word) {
        $scope.infoBar= true;
        $scope.newList = false;
        WebSQL.createWordInFavList(favList.id,word.id);
        ngDialog.closeAll();
        if($rootScope.lang == 'fra'){
            toastr.success('Ajouté avec succès à la liste');
        } else {
            toastr.success('Added with success in the list');
        }
    }

    $scope.loadMore = function() {
        // console.log($scope.tabWordsHome.length);
        if($scope.loadMoreOn) {
            var last = $scope.tabWordsHome.length-1;
            for(var i = 1; i < 50; i++) {
                if($rootScope.tabWords.length > last+i){
                    // console.log($rootScope.tabWords[last+i]);
                    $scope.tabWordsHome.push($rootScope.tabWords[last+i]);
                }
            }
            $timeout(function(){
                $scope.showLetterTitle();
            });
        }
    }

    $rootScope.currentCateg = null;

    $scope.tabWordsHome= [];
    $scope.loadMore();
    $scope.tabCategsHome = [];
    $scope.loaderHidden = true;
    $scope.newList = false;
    $scope.searchWord = "";
    $scope.tabLetters = [];
    if($rootScope.lang == undefined){
        $rootScope.lang = 'fra';
    }
    $scope.hideLetterEmpty();

    $scope.$watch('searchWord', function(newValue, oldValue){
        if(newValue != undefined && newValue != "" && newValue.length >= 3){
            $scope.tabWordsHome = $rootScope.tabWords;
        }
        if(oldValue != undefined){
            if(oldValue.length == 3 && newValue == undefined){
                $scope.tabWordsHome = [];
                $scope.loadMore();
            }
        }
        $timeout(function(){
            $scope.showLetterTitle();
        });
    });

    $scope.$watch('currentCateg', function(newValue,oldValue){
        $scope.loaderHidden = true;
        if(newValue>0){
            $scope.tabWordsHome = $rootScope.tabWords;
            $timeout(function(){
                $scope.showLetterTitle();
            },200);
        }
        if(newValue == null && oldValue>0){
            $scope.tabWordsHome = [];
            $scope.loadMore();
        }
    });
    $scope.$watch('openCateg', function(newValue,oldValue){
        if(newValue){
            $scope.tabCategsHome = $rootScope.tabCategs;
        }
    });




}]);
