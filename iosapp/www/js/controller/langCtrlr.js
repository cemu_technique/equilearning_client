app.controller('langCtrlr', ['$rootScope','$scope', '$location','LocalStorage', function($rootScope,$scope, $location, LocalStorage) {
	window.scrollTo(0,0);
    $scope.selectLanguage = function(lang){
        LocalStorage.setLang(lang).then(function(){
            $location.path('/')
        });
    }
}]);
